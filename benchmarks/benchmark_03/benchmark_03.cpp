// mylib
#include "../../mylibrary/benchmark.h"
#include "../../mylibrary/selection_sort.h"

// stl
#include <chrono>
#include <vector>
#include <stdexcept>
#include <algorithm>




///
///
///   This program benchmarks run times when sorting
///
///







template <class Container>
size_t
time_std_sort( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  selection_sort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}


int main(int /*argc*/, char** /*argv*/) try {

  namespace bm = mylib::benchmark;

  std::vector<size_t> fill_sizes {
    20
    ,100
    ,1000
    ,10000
//    ,20000
//    ,30000
//    ,40000
//    ,50000
//    ,100000
  };


  std::cout << "Benchmarking sorting!" << std::endl;

  std::vector<bm::TimingData> timing_data_std_sort;
  for( const auto& fill_size : fill_sizes ) {

    std::cout << "  Generating data set N = " << fill_size << std::endl;
    auto C = bm::genDataTestSet<int>(fill_size);

    // Time std::sort
    std::cout << "    Timing mylib::selection_sort..." << std::endl;
    timing_data_std_sort.push_back( 
      bm::TimingData(fill_size,
        time_std_sort(C)
      ) 
    );
  }
 

  // Write to file
  bm::writeToDat(timing_data_std_sort, "selection_sort");

  return 0;
}
catch (std::ifstream::failure e) {
  std::cerr << "Exception opening/reading/closing file: " << e.what();
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
