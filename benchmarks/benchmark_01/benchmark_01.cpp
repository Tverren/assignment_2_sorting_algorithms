// mylib
#include "../../mylibrary/benchmark.h"
#include "../../mylibrary/vector.h"

// stl
#include <chrono>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>



///
///
///   This program benchmarks run times when filling our custom vector with data.
///
///


template <typename Elem>
size_t
time_fill_no_reserve( typename mylib::Vector<Elem>::size_type n, const Elem& e = Elem() ) {

  using namespace mylib;

  Vector<Elem> vec{};

  auto start = std::chrono::high_resolution_clock::now();
  for( size_t i {0}; i < n; ++i )
    vec.push_back(e);
  auto end = std::chrono::high_resolution_clock::now();


  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <typename Elem>
size_t
time_fill_with_reserve( typename mylib::Vector<Elem>::size_type n, const Elem& e = Elem() ) {

  using namespace mylib;

  Vector<Elem> vec;

  auto start = std::chrono::high_resolution_clock::now();
  vec.reserve(n);
  for( size_t i {0}; i < n; ++i )
    vec.push_back(e);
  auto end = std::chrono::high_resolution_clock::now();


  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <typename Elem>
size_t
time_fill_std_with_reserve_and_emplace( typename mylib::Vector<Elem>::size_type n, const Elem& e = Elem() ) {

  using namespace std;

  vector<Elem> vec;

  auto start = std::chrono::high_resolution_clock::now();
  vec.reserve(n);
  for( size_t i {0}; i < n; ++i )
    vec.emplace_back(e);
  auto end = std::chrono::high_resolution_clock::now();


  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}






int main(int /*argc*/, char** /*argv*/) try {

  namespace bm = mylib::benchmark;

  const int fill_value {0};
  
  std::vector<int> fill_sizes {
    10000,
    100000,
    1000000                  // 4MB
//    ,10000000
//    ,100000000
//    ,1000000000               // 4GB (given element type is 4B)
  };

  std::vector<bm::TimingData> timing_data_fill_no_reserve;
  std::vector<bm::TimingData> timing_data_fill_with_reserve;
  std::vector<bm::TimingData> timing_data_fill_std_with_reserve_and_emplace;
  for( const auto& fill_size : fill_sizes ) {
    timing_data_fill_with_reserve.push_back( bm::TimingData(fill_size, time_fill_with_reserve( fill_size, fill_value ) ) );
    timing_data_fill_std_with_reserve_and_emplace.push_back( bm::TimingData(fill_size, time_fill_std_with_reserve_and_emplace( fill_size, fill_value ) ) );
  }
 
  bm::writeToDat(timing_data_fill_with_reserve,                 "Vector_push_back_reserve");
  bm::writeToDat(timing_data_fill_std_with_reserve_and_emplace, "std_vector_emplace_back_reserve");

  return 0;
}
catch (std::ifstream::failure e) {
  std::cerr << "Exception opening/reading/closing file: " << e.what();
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
