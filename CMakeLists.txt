cmake_minimum_required(VERSION 3.0)

# Set a project name and version number
project(HelloWorld VERSION 1.0)

# Global compile flags
set(MY_COMPILE_FLAGS "--std=c++11 -pedantic -Wall -Wextra")

# Unit testing
find_package(GTest REQUIRED)
if(GTEST_FOUND)

  include_directories(${GTEST_INCLUDE_DIRS})
  enable_testing()

  # Helper function
  # Invariant the named TEST SET is located in a subdirectory named 'tests'
  function(ADD_TESTS TEST_SET_NAME)
    add_executable( test_${TEST_SET_NAME} tests/${TEST_SET_NAME}.cpp )
    target_link_libraries(test_${TEST_SET_NAME} ${ARGN} ${GTEST_LIBRARIES} ${GTEST_MAIN_LIBRARIES} pthread)
    set_target_properties(test_${TEST_SET_NAME} PROPERTIES COMPILE_FLAGS ${MY_COMPILE_FLAGS})
    gtest_add_tests( test_${TEST_SET_NAME} "" tests/${TEST_SET_NAME}.cpp )
    add_dependencies( test_${TEST_SET_NAME} ${ARGN} )
  endfunction(ADD_TESTS)

endif(GTEST_FOUND)


#####
# Let us build our library

# Add library as subdirectory
add_subdirectory(mylibrary)


##### 
# Let us build a hellow world program using a function from our library

# List source files
set(SRCS
  hello_world.cpp)

# Add rules to create an executable 
add_executable(${CMAKE_PROJECT_NAME} ${SRCS})

# Tell the compiler to use the our compile flags
set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES COMPILE_FLAGS ${MY_COMPILE_FLAGS})

# Link our target executable with the libraries we use
target_link_libraries(${CMAKE_PROJECT_NAME} mylibrary)

# Make sure the our library is built befor our hello world application
# Default behaviour as the add_executable rule is listed after the add_library rule from the library subdirectory
add_dependencies(${CMAKE_PROJECT_NAME} mylibrary)


#####
# Let us a benchmark directory
add_subdirectory(benchmarks)


#####
# Let us build our report
option(BUILD_REPORT "Build a latex report..." FALSE)
if(BUILD_REPORT)
#  add_subdirectory(report)
  add_subdirectory(report_selection_sort)
  add_subdirectory(report_introsort)
endif(BUILD_REPORT)





