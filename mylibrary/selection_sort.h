#ifndef SELECTION_SORT_H
#define SELECTION_SORT_H

#include <functional>

namespace mylib {

  template <class RandomAccessIterator,
            class Compare = std::less<typename RandomAccessIterator::value_type>>
  void selection_sort ( RandomAccessIterator first,
                        RandomAccessIterator last,
                        Compare cmp = Compare() ) {

    using size_type = typename RandomAccessIterator::difference_type;
    size_type n {last - first};

    for( size_type i {0}; i < n; ++i ) {

      size_type j {i};
      size_type k {j+1};
      for( ; k < n; ++k )
        if( cmp(first[k],first[j]) ) j = k;

      if(i!=j)
        std::swap( first[i], first[j] );
    }

  }
}




#endif // SELECTION_SORT_H
