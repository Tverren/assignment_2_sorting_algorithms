#ifndef INTROSORT
#define INTROSORT

#include <cmath>
#include <functional>
#include <iterator>
#include <algorithm>


#include "shellsort.h"
#include "selection_sort.h"


namespace mylib{

    //Median of 3
     template <class RandomAccessIterator>
     typename RandomAccessIterator::value_type median_of_3( RandomAccessIterator Medo3First,
                                                            RandomAccessIterator Medo3Middle,
                                                            RandomAccessIterator Medo3Last){

         if(Medo3First > Medo3Middle)
              std::swap(Medo3First, Medo3Middle);

         if(Medo3First > Medo3Last)
              std::swap(Medo3First, Medo3Last);

         if(Medo3Middle > Medo3Last)
             std::swap(Medo3Middle, Medo3Last);

         return *Medo3Middle;
     }

    //introsort_loop
    template <class RandomAccessIterator>
    void introsort_loop( RandomAccessIterator first,
                         RandomAccessIterator last,
                         typename RandomAccessIterator::difference_type depth_limit){

        using value_type = typename RandomAccessIterator::value_type;

        while(last-first > 0){
            if(depth_limit == 0){
               bubbleSort(first, last);
               //heapSort(first, last);
               return;
            }//END if
            depth_limit = depth_limit-1;
            value_type mo3 = median_of_3(first, (first+(last-first)/2), (last-1));
            RandomAccessIterator p = std::partition(
                        first,
                        last,
                        [mo3](value_type e){return mo3 < e; }
                       );
            introsort_loop(p, last, depth_limit);
            last = p;
        }//End while

    }//END introsort_loop

    //introsort
    template <class RandomAccessIterator,
              class Compare = std::less<typename RandomAccessIterator::value_type>>
    void introsort ( RandomAccessIterator first,
                     RandomAccessIterator last,
                     Compare cmp = Compare()) {

        using size_type = typename RandomAccessIterator::difference_type;
        size_type N {last-first};

        introsort_loop(first, last, 2*std::floor(std::log2(N)));
        shellSort(first, last);
        //insertionSort(first, last);
    }//END introsort
} //END namespace
#endif // INTROSORT
