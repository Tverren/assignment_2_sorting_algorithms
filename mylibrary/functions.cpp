#include "functions.h"

#include <iostream>
#include <string>

namespace mylib {

  void helloWorld(const std::string& words) {

    std::cout << "Hello world!" << std::endl << "Heed my words: " << words << std::endl;
  }

} // END namespace mylib
